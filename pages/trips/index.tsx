import React, { useEffect, useState } from "react";
import "@tomtom-international/web-sdk-maps/dist/maps.css";
import Sidebar from "../../components/Sidebar";
import Controls from "./Controls";

export default function Index() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  useEffect(() => {
    const tt = require("@tomtom-international/web-sdk-maps");
    const map = tt.map({
      key: process.env.NEXT_PUBLIC_TOMTOM_API_KEY,
      container: "map",
    });
  }, []);

  return (
    <div className="absolute top-0 left-0 w-full h-full grid grid-flow-col md:grid-cols-3 grid-rows-1 overflow-auto p-inherit">
      <Controls className="hidden md:flex" />
      <Sidebar
        show={sidebarOpen}
        className="absolute w-full h-full p-inherit block sm:hidden"
      >
        <Controls />
      </Sidebar>
      <button
        onClick={() => setSidebarOpen((toggleState) => !toggleState)}
        className="fixed w-16 h-16 rounded-full bg-black z-30 right-2 sm:right-20 bottom-5 flex items-center justify-center sm:hidden text-white p-4"
      >
        {sidebarOpen ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 8h16M4 16h16"
            />
          </svg>
        )}
      </button>
      <div id="map" className="md:col-span-2 self-stretch"></div>
    </div>
  );
}
