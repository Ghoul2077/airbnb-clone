import React, { useState } from "react";

export default function Controls({ className = "" }: { className?: any }) {
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(100);

  return (
    <div
      className={`flex flex-col justify-between md:col-span-1 self-stretch scrollbar-thumb-rounded overflow-y-auto scrollbar-thin bg-white sm:bg-transparent ${className}`}
    >
      <div className="bg-gray-100 p-4 flex justify-center lg:px-6">
        <div className="w-full max-w-3xl mx-auto">
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <label
                htmlFor="check_in"
                className="block text-md leading-6 font-medium text-gray-500 mb-3"
              >
                Check in
              </label>
              <input
                type="date"
                name="check_in"
                id="check_in"
                placeholder="DD-MM-YYYY"
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md p-3 focus:outline-none"
              />
            </div>
            <div className="col-span-6 sm:col-span-3">
              <label
                htmlFor="check_out"
                className="block text-md leading-6 font-medium text-gray-500 mb-3"
              >
                Check out
              </label>
              <input
                type="date"
                name="check_out"
                id="check_out"
                placeholder="DD-MM-YYYY"
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md p-3 focus:outline-none"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="bg-gray-100 p-4 flex justify-center lg:px-6">
        <div className="w-full max-w-3xl mx-auto">
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <label
                htmlFor="first_name"
                className="block text-md leading-6 font-medium text-gray-500 mb-3"
              >
                First name
              </label>
              <input
                type="text"
                name="first_name"
                id="first_name"
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md p-3 focus:outline-none"
              />
            </div>

            <div className="col-span-6 sm:col-span-3">
              <label
                htmlFor="last_name"
                className="block text-md leading-6 font-medium text-gray-500 mb-3"
              >
                Last name
              </label>
              <input
                type="text"
                name="last_name"
                id="last_name"
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md p-3 focus:outline-none"
              />
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="p-4 border-b border-gray-200 sm:px-6">
          <h3 className="text-md leading-6 font-medium text-gray-500">
            Price Range
          </h3>
        </div>
        <div className="bg-gray-100 p-4 flex justify-center lg:px-6">
          <div className="w-full max-w-3xl mx-auto">
            <fieldset>
              <legend className="sr-only">Price Range</legend>
              <label htmlFor="min_price" className="text-gray-600 text-sm">
                Min Price : {minPrice}
              </label>
              <input
                id="min_price"
                className="rounded-lg overflow-hidden appearance-none bg-gray-400 h-3 w-128 focus:outline-none w-full my-5"
                type="range"
                min="1"
                max="100"
                step="1"
                value={minPrice}
                onChange={(e) => {
                  const value = Number(e.target.value);
                  if (value < maxPrice) setMinPrice(value);
                }}
              />
              <label htmlFor="max_price" className="text-gray-600 text-sm">
                Max Price : {maxPrice}
              </label>
              <input
                id="max_price"
                className="rounded-lg overflow-hidden appearance-none bg-gray-400 h-3 w-128 focus:outline-none w-full my-5"
                type="range"
                min="1"
                max="100"
                step="1"
                value={maxPrice}
                onChange={(e) => {
                  const value = Number(e.target.value);
                  if (value > minPrice) setMaxPrice(value);
                }}
              />
            </fieldset>
          </div>
        </div>
      </div>
      <div>
        <div className="p-4 border-b border-gray-200 sm:px-6">
          <h3 className="text-md leading-6 font-medium text-gray-500">
            Home Type
          </h3>
        </div>
        <div className="bg-gray-100 p-4 flex justify-center lg:px-6">
          <div className="w-full max-w-3xl mx-auto">
            <fieldset>
              <legend className="sr-only">Home Type</legend>

              <div className="bg-white rounded-md -space-y-px">
                <div className="relative border rounded-tl-md rounded-tr-md p-4 flex">
                  <div className="flex items-center h-5">
                    <input
                      id="settings-option-0"
                      name="home_type"
                      type="radio"
                      className="focus:ring-indigo-500 text-indigo-600 cursor-pointer border-gray-300 mt-1.5"
                      defaultChecked
                    />
                  </div>
                  <label
                    htmlFor="settings-option-0"
                    className="ml-3 flex flex-col cursor-pointer w-full"
                  >
                    <span className="block text-md font-medium text-gray-800">
                      Entire Place
                    </span>
                    <span className="block text-sm text-gray-500">
                      Have a place to yourself
                    </span>
                  </label>
                </div>

                <div className="relative border border-gray-200 p-4 flex">
                  <div className="flex items-center h-5">
                    <input
                      id="settings-option-1"
                      name="home_type"
                      type="radio"
                      className="focus:ring-indigo-500 text-indigo-600 cursor-pointer border-gray-300 mt-1.5"
                    />
                  </div>
                  <label
                    htmlFor="settings-option-1"
                    className="ml-3 flex flex-col cursor-pointer w-full"
                  >
                    <span className="block text-md font-medium text-gray-800">
                      Private Room
                    </span>
                    <span className="block text-sm text-gray-500">
                      Have your own room and share some spaces
                    </span>
                  </label>
                </div>
                <div className="relative border border-gray-200 rounded-bl-md rounded-br-md p-4 flex">
                  <div className="flex items-center h-5">
                    <input
                      id="settings-option-2"
                      name="home_type"
                      type="radio"
                      className="focus:ring-indigo-500 text-indigo-600 cursor-pointer border-gray-300 mt-1.5"
                    />
                  </div>
                  <label
                    htmlFor="settings-option-2"
                    className="ml-3 flex flex-col cursor-pointer w-full"
                  >
                    <span className="block text-md font-medium text-gray-800">
                      Shared Room
                    </span>
                    <span className="block text-sm text-gray-500">
                      Stay in a shared space, like a common room
                    </span>
                  </label>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
  );
}
