import "tailwindcss/tailwind.css";
import { useEffect, useState } from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import Router from "next/router";
import Loader from "../components/Loader";
import { NotificationProvider } from "../context/notification";
import Navbar from "../components/Navbar";

function MyApp({ Component, pageProps }: AppProps) {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const start = () => {
      setLoading(true);
    };
    const end = () => {
      setLoading(false);
    };

    Router.events.on("routeChangeStart", start);
    Router.events.on("routeChangeComplete", end);
    Router.events.on("routeChangeError", end);

    return () => {
      Router.events.off("routeChangeStart", start);
      Router.events.off("routeChangeComplete", end);
      Router.events.off("routeChangeError", end);
    };
  }, []);

  return (
    <>
      <Head>
        <title>Airbnb Clone</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <NotificationProvider>
        <div className="flex flex-col bg-gray-100 relative min-h-screen pt-20">
          <Navbar />
          <Loader show={loading} />
          <Component {...pageProps} />
        </div>
      </NotificationProvider>
    </>
  );
}

export default MyApp;
