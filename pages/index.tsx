import React from "react";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import Content from "../components/Content";

const Index = () => (
  <>
    <Hero />
    <div className="py-10">
      <Content />
      <Content />
      <Content />
    </div>
    <Footer />
  </>
);

export default Index;
