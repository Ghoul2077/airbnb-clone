import React from "react";
import { useRouter } from "next/router";

export default function Index() {
  const router = useRouter();
  const { id } = router.query;

  return (
    <div className="flex flex-1 justify-center items-center">
      Home {id} details page here
    </div>
  );
}
