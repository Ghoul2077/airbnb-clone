import { Transition } from "@tailwindui/react";
import React, {
  createContext,
  useState,
  useContext,
  useEffect,
  useRef,
} from "react";

export const NotificationContext = createContext(null);
export const useNotification = () => useContext(NotificationContext);

const NOTIFICATION_DISMISS_TIME = 5000;

export function NotificationProvider({ children }) {
  const timerRef = useRef(null);
  const [notificationStack, setNotificationStack]: any = useState([]);

  useEffect(() => {
    if (notificationStack?.length) {
      timerRef.current = setTimeout(
        () =>
          setNotificationStack((notifications: string | any[]) =>
            notifications.slice(1)
          ),
        NOTIFICATION_DISMISS_TIME
      );
    }

    return () => clearTimeout(timerRef.current);
  }, [notificationStack]);

  function showNotification({ message, label, action }) {
    notificationStack.push({ message, label, action });
  }

  function closeNotification(index: number) {
    setNotificationStack((notifications: any) => [
      ...notifications.slice(0, index),
      ...notifications.slice(index + 1),
    ]);
  }

  return (
    <NotificationContext.Provider value={{ showNotification }}>
      <div className="fixed bottom-0 right-0 sm:w-96 xs:w-80 w-72 flex flex-col items-end justify-center px-4 py-6 pointer-events-none sm:p-6 sm:items-start sm:justify-end z-50">
        {notificationStack.map((notification, index) => (
          <Transition
            key={index}
            show={true}
            appear={true}
            enter="transition ease-out duration-300"
            enterFrom="transform opacity-0 scale-0"
            enterTo="transform opacity-100 scale-100"
            className="w-full"
          >
            <div className="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden my-2">
              <div className="p-4">
                <div className="flex items-center">
                  <div className="w-0 flex-1 flex justify-between">
                    <p className="w-0 flex-1 text-sm font-medium text-gray-900">
                      {notification.message}
                    </p>
                    <button
                      className="ml-3 flex-shrink-0 bg-white rounded-md text-sm font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                      onClick={notification.action}
                    >
                      {notification.label}
                    </button>
                  </div>
                  <div className="ml-4 flex-shrink-0 flex">
                    <button
                      className="bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                      onClick={() => closeNotification(index)}
                    >
                      <span className="sr-only">Close</span>
                      <svg
                        className="h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        ))}
      </div>
      {children}
    </NotificationContext.Provider>
  );
}
