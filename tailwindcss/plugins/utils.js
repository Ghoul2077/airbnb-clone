const plugin = require("tailwindcss/plugin");

module.exports = plugin(function ({ addUtilities }) {
  const newUtilities = {
    ".p-inherit": {
      padding: "inherit",
    },
    ".min-h-inherit": {
      "min-height": "inherit",
    },
  };

  addUtilities(newUtilities);
});
