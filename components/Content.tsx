import React from "react";
import Card from "./Card";

export default function Content() {
  return (
    <div className="w-full h-full lg:px-12">
      <div className="md:px-8 md:py-6 p-3">
        <div className="flex items-center justify-between">
          <div className="flex-1 min-w-0">
            <h2 className="text-2xl leading-7 text-gray-900 sm:text-3xl sm:truncate">
              <span className="text-gray-500">Apartments in</span> Barcelona,
              Spain
            </h2>
          </div>
          <div className="flex md:mt-0 md:ml-4">
            <button
              type="button"
              className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Show All
            </button>
          </div>
        </div>
        <div className="mt-12 max-w-lg min-w-full grid gap-11 xl:grid-cols-4 lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 lg:max-w-none">
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </div>
      </div>
    </div>
  );
}
