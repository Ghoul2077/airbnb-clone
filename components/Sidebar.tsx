import { Transition } from "@tailwindui/react";
import React from "react";

export default function Sidebar({ children, show, ...otherProps }: any) {
  return (
    <Transition
      show={show}
      appear={true}
      enter="transition-opacity ease-linear duration-300"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition-opacity ease-linear duration-300"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
      {...otherProps}
    >
      <div className="fixed inset-0 overflow-hidden p-inherit bg-black bg-opacity-50 z-20">
        <Transition.Child
          enter="transition-opacity duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          className="p-inherit"
        >
          <Transition.Child
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
            className="p-inherit"
          >
            <section className="bg-transparent shadow-xl absolute inset-y-0 max-w-full flex sm:pl-16 p-inherit right-0 h-screen overflow-y-auto">
              <div className="w-screen max-w-2xl bg-white">{children}</div>
            </section>
          </Transition.Child>
        </Transition.Child>
      </div>
    </Transition>
  );
}
