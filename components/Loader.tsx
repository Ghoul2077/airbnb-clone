import React from "react";
import Lottie from "react-lottie";
import animationData from "../public/lotties/loader.json";

export default function Loader({ show = false }: { show?: boolean }) {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return show ? (
    <div className="fixed h-full w-full bg-white flex justify-center items-center overflow-hidden z-30">
      <div className="pointer-events-none">
        <Lottie
          options={defaultOptions}
          height={300}
          width={300}
          isClickToPauseDisabled={true}
        />
      </div>
    </div>
  ) : null;
}
