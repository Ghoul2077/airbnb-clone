import React from "react";
import Image from "next/image";
import Link from "next/link";

export default function Footer() {
  return (
    <footer
      className="absolute bg-white w-full top-full pt-5 mt-3 shadow-lg"
      aria-labelledby="footerHeading"
    >
      <h2 id="footerHeading" className="sr-only">
        Footer
      </h2>
      <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
        <div className="pb-8">
          <div className="grid md:grid-cols-2 gap-8 xl:col-span-4">
            <div className="mt-0">
              <Image
                className="block lg:hidden h-8 w-auto"
                src="/images/logo.svg"
                alt="logo"
                width={35}
                height={35}
              />
              <form className="mt-4 sm:max-w-xs">
                <fieldset className="w-full">
                  <label htmlFor="language" className="sr-only">
                    Language
                  </label>
                  <div className="relative">
                    <select
                      id="language"
                      name="language"
                      className="appearance-none block w-full bg-none bg-white border border-gray-300 rounded-md py-2 pl-3 pr-10 text-base text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    >
                      <option>English</option>
                      <option>French</option>
                      <option>German</option>
                      <option>Japanese</option>
                      <option>Spanish</option>
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 px-2 flex items-center">
                      <svg
                        className="h-4 w-4 text-gray-400"
                        data-todo-x-description="Heroicon name: solid/chevron-down"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fillRule="evenodd"
                          d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                    </div>
                  </div>
                </fieldset>
                <fieldset className="mt-4 w-full">
                  <label htmlFor="currency" className="sr-only">
                    Currency
                  </label>
                  <div className="relative">
                    <select
                      id="currency"
                      name="currency"
                      className="appearance-none block w-full bg-none bg-white border border-gray-300 rounded-md py-2 pl-3 pr-10 text-base text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    >
                      <option>ARS</option>
                      <option>Indian Rupees</option>
                      <option>CAD</option>
                      <option>CHF</option>
                      <option>EUR</option>
                      <option>GBP</option>
                      <option>JPY</option>
                      <option>USD</option>
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 px-2 flex items-center">
                      <svg
                        className="h-4 w-4 text-gray-400"
                        data-todo-x-description="Heroicon name: solid/chevron-down"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fillRule="evenodd"
                          d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                    </div>
                  </div>
                </fieldset>
              </form>
              <div className="grid md:grid-cols-2 grid-cols-1 mt-8">
                <div className="grid grid-cols-3 px-1">
                  <Link href="/">
                    <a className="md:text-left text-center text-base text-gray-600 hover:text-gray-900 focus:outline-none">
                      Terms
                    </a>
                  </Link>
                  <Link href="/">
                    <a className="md:text-left text-center text-base text-gray-600 hover:text-gray-900 focus:outline-none">
                      Privacy
                    </a>
                  </Link>
                  <Link href="/">
                    <a className="md:text-left text-center text-base text-gray-600 hover:text-gray-900 focus:outline-none">
                      Sitemap
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="md:grid md:grid-cols-3 md:gap-8">
              <div>
                <h3 className="md:text-left text-center text-sm font-semibold text-gray-400 tracking-wider uppercase">
                  About us
                </h3>
                <ul className="mt-4 space-y-4">
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        About us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Careers
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Path
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Help
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="mt-4 md:mt-0">
                <h3 className="md:text-left text-center text-sm font-semibold text-gray-400 tracking-wider uppercase">
                  Discover
                </h3>
                <ul className="mt-4 space-y-4">
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Trust & Safety
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Travel Credit
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Gift Cards
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Businnes Travel
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="mt-4 md:mt-0">
                <h3 className="md:text-left text-center text-sm font-semibold text-gray-400 tracking-wider uppercase">
                  Hosting
                </h3>
                <ul className="mt-4 space-y-4">
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Why host
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Hospitality
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Hosting
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a className="text-base text-gray-600 hover:text-gray-900">
                        Community
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
