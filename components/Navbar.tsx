import React, { useEffect, useState } from "react";
import { Transition } from "@tailwindui/react";
import { useRouter } from "next/dist/client/router";
import Link from "next/link";
import Image from "next/image";

const NAV_MENU_ITEMS = [
  { label: "Become a host", url: "/host" },
  { label: "Trips", url: "/trips" },
  { label: "Messages", url: "/messages" },
  { label: "Help", url: "/help" },
];

const PROFILE_MENU_ITEMS = [
  { label: "Your Profile", url: "/" },
  { label: "Settings", url: "/" },
  { label: "Sign Out", url: "/" },
];

export default function Navbar() {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [profilePopupOpen, setProfilePopupOpen] = useState(false);
  const [activeIndex, setActiveIndex] = useState(
    NAV_MENU_ITEMS.findIndex((item) => item.url === router.pathname)
  );

  useEffect(() => {
    setActiveIndex(
      NAV_MENU_ITEMS.findIndex((item) => item.url === router.pathname)
    );
  }, [router.pathname]);

  const toggleMenu = () => setIsOpen((status) => !status);
  const toggleProfilePopup = () => setProfilePopupOpen((status) => !status);

  return (
    <nav className="bg-white fixed w-full shadow z-40 top-0">
      <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
        <div className="flex justify-between h-20 w-full">
          <div className="flex px-2 lg:px-0">
            <div className="flex-shrink-0 flex items-center">
              <Link href="/">
                <a className="focus:outline-none">
                  <Image
                    className="block lg:hidden h-8 w-auto cursor-pointer"
                    src="/images/logo.svg"
                    alt="logo"
                    width={35}
                    height={35}
                  />
                </a>
              </Link>
            </div>
            <div className="flex-1 flex sm:w-96 items-center px-2 lg:justify-start ml-5">
              <div className="max-w-lg w-full">
                <label htmlFor="search" className="sr-only">
                  Search
                </label>
                <div className="relative">
                  <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <svg
                      className="h-5 w-5 text-gray-400"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                    >
                      <path
                        fillRule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  <input
                    id="nav-search"
                    className="block w-full pl-10 pr-3 py-2 border border-gray-300 rounded-md leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:border-blue-300 focus:shadow-outline-blue sm:text-sm"
                    placeholder="Search for rooms, apartments or homes..."
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="flex items-center lg:hidden">
            <button
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500"
              onClick={toggleMenu}
            >
              <svg
                className="h-6 w-6"
                stroke="currentColor"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  className="inline-flex"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h16"
                />
                <path
                  className="hidden"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
          <div className="hidden lg:ml-4 lg:flex lg:items-center">
            <div className="hidden lg:ml-6 lg:flex h-full">
              {NAV_MENU_ITEMS.map((item, index) => (
                <Link href={item.url} key={index}>
                  <a
                    className={`ml-8 inline-flex justify-center items-center px-1 pt-1 border-indigo-500 ${
                      activeIndex === index
                        ? "border-b-2  font-semibold"
                        : "border-b-0"
                    } text-base leading-5 text-gray-900 focus:outline-none focus:border-indigo-700`}
                    onClick={() => setActiveIndex(index)}
                  >
                    {item.label}
                  </a>
                </Link>
              ))}
            </div>
            <div className="ml-10 relative flex-shrink-0 h-12 w-12">
              <button
                className="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 w-full h-full"
                onClick={toggleProfilePopup}
              >
                <Image
                  className="rounded-full"
                  src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  alt="profile"
                  layout="fill"
                />
              </button>
              <Transition
                show={profilePopupOpen}
                enter="transition ease-out duration-75"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-150"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
                className="absolute right-0 w-56 mt-2 origin-top-right rounded-md shadow-lg z-10"
              >
                <div className="origin-top-right absolute right-0 mt-4 w-48 rounded-md shadow-lg">
                  <div className="py-1 rounded-md bg-white shadow-xs">
                    {PROFILE_MENU_ITEMS.map((item, index) => (
                      <Link href={item.url} key={index}>
                        <a className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                          {item.label}
                        </a>
                      </Link>
                    ))}
                  </div>
                </div>
              </Transition>
            </div>
          </div>
        </div>
      </div>
      <Transition
        show={isOpen}
        enter="transition-opacity ease-linear duration-200"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity ease-linear duration-300"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className={`${isOpen ? "block" : "hidden"} lg:hidden`}>
          <div className="pt-2 pb-3">
            {NAV_MENU_ITEMS.map((item, index) => (
              <Link href={item.url} key={index}>
                <a
                  href={item.url}
                  className={`mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none ${
                    activeIndex === index &&
                    "text-gray-800 bg-gray-50 border-gray-300"
                  }`}
                  onClick={() => setActiveIndex(index)}
                >
                  {item.label}
                </a>
              </Link>
            ))}
          </div>
          <div className="pt-4 pb-3 border-t border-gray-200">
            <div className="flex items-center px-4">
              <div className="flex-shrink-0 relative h-10 w-10">
                <Image
                  className="rounded-full"
                  src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  alt="profile"
                  layout="fill"
                />
              </div>
              <div className="ml-3">
                <div className="text-base font-medium leading-6 text-gray-800">
                  Tom Cook
                </div>
                <div className="text-sm font-medium leading-5 text-gray-500">
                  tom@example.com
                </div>
              </div>
            </div>
            <div className="mt-3">
              {PROFILE_MENU_ITEMS.map((item, index) => (
                <Link href={item.url} key={index}>
                  <a className="mt-1 block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none">
                    {item.label}
                  </a>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </Transition>
    </nav>
  );
}
