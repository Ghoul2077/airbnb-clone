import React from "react";
import Link from "next/link";
import Image from "next/image";

export default function Card() {
  return (
    <Link href={`${Math.floor(Math.random() * 100)}`}>
      <a className="flex flex-col rounded-lg overflow-hidden">
        <div className="flex-shrink-0">
          <Image
            src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1679&amp;q=80"
            alt="hotel image"
            width={400}
            height={350}
            layout="responsive"
          />
        </div>
        <div className="flex-1 py-4 px-0 flex flex-col justify-between">
          <div className="flex-1">
            <div className="flex justify-between">
              <p className="text-sm font-medium text-indigo-600">
                $123 per night
              </p>
              <p className="ml-auto flex items-center text-sm font-semibold text-green-400">
                <span className="w-4 mr-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
                    />
                  </svg>
                </span>
                4.8
              </p>
            </div>
            <div className="block mt-2">
              <p className="text-lg font-semibold text-gray-700 md:mr-16 lg:mr-5 hover:underline">
                Explore Old Barcelona from a Loft-Style Studio
              </p>
            </div>
          </div>
        </div>
      </a>
    </Link>
  );
}
