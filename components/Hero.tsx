import React, { useState } from "react";
import Image from "next/image";

const SEARCH_FILTERS = [
  "All Properties",
  "Homes",
  "Experiences",
  "Restaurants",
];

export default function Hero() {
  const [filterStatus, setFilterStatus] = useState(
    Array(SEARCH_FILTERS.length).fill(false)
  );

  function toggleFilterStatus(index: number) {
    setFilterStatus((filterStatus) => [
      ...filterStatus.slice(0, index),
      !filterStatus[index],
      ...filterStatus.slice(index + 1),
    ]);
  }

  return (
    <div className="bg-white w-full h-full flex lg:flex-row flex-col justify-between items-center">
      <div className="flex-1 flex flex-col lg:items-start items-center md:px-20 sm:px-10 px-4">
        <Image
          src="/images/welcome.svg"
          alt="hero"
          height={350}
          width={350}
          layout="intrinsic"
        />
        <p className="lg:text-5xl md:text-4xl text-3xl leading-normal lg:text-left text-center">
          Book unique homes and experiences all over the world.
        </p>
        <div className="relative flex items-center lg:max-w-xl w-full">
          <input
            id="hero-search"
            className="block w-full mt-12 lg:p-5 p-4 border border-gray-600 rounded-md leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:border-blue-800 focus:shadow-outline-blue sm:text-md"
            placeholder="Try “Copenhagen, Denmark”"
          />
          <button onClick={() => console.log("searching")}>
            <div className="absolute inset-y-0 right-0 flex mt-12 p-4 items-center">
              <svg
                className="h-5 w-5 text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
              >
                <path
                  fillRule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          </button>
        </div>
        <div className="sm:py-8 py-3 bg-white flex items-center justify-center">
          <span className="relative z-0 inline-flex rounded-md flex-wrap">
            {SEARCH_FILTERS.map((filter, index) => (
              <button
                key={index}
                type="button"
                className={`-ml-px relative inline-flex items-center px-4 py-2 rounded-md border border-gray-300 bg-gray-400 md:text-md text-sm sm:w-auto font-medium text-white hover:bg-gray-500 ${
                  filterStatus[index] && "bg-green-400"
                } focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 mr-3`}
                onClick={() => toggleFilterStatus(index)}
              >
                {filter}
              </button>
            ))}
          </span>
        </div>
      </div>
      <div className="hidden lg:block flex-1 relative self-stretch">
        <Image
          src="/images/hero.png"
          alt="hero"
          layout="fill"
          objectFit="cover"
        />
      </div>
    </div>
  );
}
